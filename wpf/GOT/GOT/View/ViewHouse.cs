﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOT.View
{
    class ViewHouse
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private readonly housemodel _housemodel;

        public int Id
        {
            get { return _housemodel.Id; }
            set { _housemodel.Id = value; }
        }

        public string Name
        {
            get { return _housemodel.Name; }
            set { _housemodel.Name = value;
                OnPropertyChanged("NumbersOfUnities");
                OnPropertyChanged("Leader");
            }
        }

        public int NumbersOfUnities
        {
            get { return _housemodel.NumbersOfUnities; }
            set {
                _housemodel.NumbersOfUnities = value;
                OnPropertyChanged("Name");
                OnPropertyChanged("Leader");
            }
        }

        public int Leader
        {
            get { return _housemodel.Leader; }
            set { _housemodel.Leader = value;

                OnPropertyChanged("Name");
                OnPropertyChanged("Leader");
            }
        }
        public ViewHouse()
        {
            _housemodel = new housemodel();
            Leader = -1;
        }
        

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
