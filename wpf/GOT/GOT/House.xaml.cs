﻿using GOT.View;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GOT
{
   
    /// <summary>
    /// Logique d'interaction pour House.xaml
    /// </summary>
    public partial class House : Window
    {
        public IEnumerable<housemodel> _House { get; set; }
        public House(IEnumerable<housemodel> Houses)
        {
            InitializeComponent();
            _House = Houses;
            Loaded += MainWindow_Loaded;
            btnEditer.Click += Edit_Click;
            btnAjouter.Click += Add_Click;
            listHouse.SelectionChanged += listHouse_SelectionChanged;
        }

        private async void Add_Click(object sender, RoutedEventArgs e)
        {
            ViewHouse house = gridEdition.DataContext as ViewHouse;
            housemodel newhouse = new housemodel();
            newhouse.Id = -1;
            newhouse.Name = house.Name;
            newhouse.NumbersOfUnities = house.NumbersOfUnities;
            newhouse.Leader = house.Leader;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:33992/");
                string jsonObject = JsonConvert.SerializeObject(house);
                HttpContent content = new StringContent(jsonObject.ToString(), System.Text.Encoding.UTF8, "application/json");
                var result = await client.PostAsync("api/House/modify", content);
            }
            this.Close();

        }

        private void listHouse_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            gridEdition.DataContext = listHouse.SelectedItem;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ViewHouse viewhouse = new ViewHouse();
            this.DataContext = viewhouse;
            listHouse.ItemsSource = _House;
        }

        private async void Edit_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable houses = listHouse.Items.SourceCollection;
            foreach (housemodel house in houses)
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:33992/");
                    string jsonObject = JsonConvert.SerializeObject(house);
                    HttpContent content = new StringContent(jsonObject.ToString(), System.Text.Encoding.UTF8, "application/json");
                    var result = await client.PostAsync("api/House/modify", content);
                }
            }
            this.Close();
        }
       
    }
}
