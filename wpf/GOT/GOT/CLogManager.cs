﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOT
{
    class CLogManager
    {
        public void Write(string s )
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            path = System.IO.Path.GetFullPath(path + @"\..\..\error.txt");
            string[] text =System.IO.File.ReadAllLines(path);
            var test = text.ToList();
            test.Add(s);
            text = test.ToArray();
            System.IO.File.WriteAllLines(path, text);
        }
    }
}
