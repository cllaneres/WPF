﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOT
{
    [SerializableAttribute]
    public class housemodel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumbersOfUnities { get; set; }
        public int Leader { get; set; }
        public housemodel() { }
        public housemodel(int id, string name, int numbersOfUnites, int leader)
        {
            this.Id = id;
            this.Name = name;
            this.NumbersOfUnities = numbersOfUnites;
            this.Leader = leader;
        }
        public override string ToString()
        {
            return this.Name + " => " + this.NumbersOfUnities;
        }
    }
   

}
