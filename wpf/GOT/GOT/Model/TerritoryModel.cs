﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOT
{
    public enum TerritoryType
    {
        SEA,
        MOUNTAIN,
        LAND,
        DESERT
    }
    public class TerritoryModel
    {
        public int Id { get; set; }
        public string TerritoryType { get; set; }
        public int Owner { get; set; }
        public string Name { get; set; }
        public TerritoryModel() { }
        public TerritoryModel(int id, string Name, string territoryType, int owner)
        {
            this.Id = id;
            this.TerritoryType = territoryType;
            this.Owner = owner;
            this.Name = Name;
        }
        public override string ToString()
        {
            return this.Name +  " -> " + this.TerritoryType;
        }

    }

}
