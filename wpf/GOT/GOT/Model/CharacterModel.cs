﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GOT
{

    public enum CharacterTypeEnum
    {
        VILLAGER,
        WARRIOR,
        WITCH,
        TACTICIAN,
        LEADER,
        LOSER
    }
    public class CharacterModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PV { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public int Strength { get; set; }
        public string Type { get; set; }
        public int house { get; set; }
        public CharacterModel() { }
        public CharacterModel(int id, string firstname, string lastname, int pv, int bravoury, int Crazyness, int Strength, string Type, int house)
        {
            this.Id = id;
            this.FirstName = firstname;
            this.LastName = lastname;
            this.PV = pv;
            this.Bravoury = bravoury;
            this.Crazyness = Crazyness;
            this.Strength = Strength;
            this.Type = Type;
            this.house = house;
        }
        public override string ToString()
        {
            return this.FirstName + " " + this.LastName+ " -> " + Type;
        }
    }

}
