﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System.Linq;
using System.Threading.Tasks;

namespace GOT
{
    /// <summary>
    /// Logique d'interaction pour Parametre.xaml
    /// </summary>
    public partial class Parametre : Window
    {
        private IEnumerable<CharacterModel> _Characters { get; set; }
        private IEnumerable<TerritoryModel> _Territory { get; set; }
        public IEnumerable<housemodel> _Houses { get; set; }
        public  Parametre()
        {
            InitializeComponent();
            InitializeTerritory();
            InitializeCharacter();
            InitializeHouse();
        }


        private async void InitializeHouse()
        {
            string temp = null;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:33992/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/house");
                if (response.IsSuccessStatusCode)
                {
                    temp = await response.Content.ReadAsStringAsync();

                }
            }
            if (temp != null)
            {
                _Houses = JsonConvert.DeserializeObject<IEnumerable<housemodel>>(temp);
            }
        }

        private async void InitializeCharacter()
        {
            string temp = null;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:33992/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/character");
                if (response.IsSuccessStatusCode)
                {
                    temp = await response.Content.ReadAsStringAsync();

                }
            }
            if (temp != null)
            {
                _Characters = JsonConvert.DeserializeObject<IEnumerable<CharacterModel>>(temp);
            }
        }

        private async void InitializeTerritory()
        {
            string temp = null;
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://localhost:33992/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = await client.GetAsync("api/territory");
                if (response.IsSuccessStatusCode)
                {
                    temp = await response.Content.ReadAsStringAsync();

                }
            }
            if (temp != null)
            {
                _Territory = JsonConvert.DeserializeObject<IEnumerable<TerritoryModel>>(temp);
            }
        }

        private void House_Click(object sender, RoutedEventArgs e)
        {

            listView.ItemsSource = _Houses;

        }

        private void Character_Click(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = _Characters;
        }

        private void Territory_Click(object sender, RoutedEventArgs e)
        {
            listView.ItemsSource = _Territory;
        }

        private void House_Exporte_Click(object sender, RoutedEventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            path = System.IO.Path.GetFullPath(path + @"\..\..\HouseXml.net");
            StreamWriter stream = new StreamWriter(path);
            XmlSerializer serializer = new XmlSerializer(typeof(List<housemodel>));
            serializer.Serialize(stream, _Houses.ToList());
            stream.Close();
        }

        private void Character_Exporte_Click(object sender, RoutedEventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            path = System.IO.Path.GetFullPath(path + @"\..\..\CharacterXml.net");
            StreamWriter stream = new StreamWriter(path);
            XmlSerializer serializer = new XmlSerializer(typeof(List<CharacterModel>));
            serializer.Serialize(stream, _Characters.ToList());
            stream.Close();
        }

        private void Territory_Exporte_Click(object sender, RoutedEventArgs e)
        {
            string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);
            path = System.IO.Path.GetFullPath(path + @"\..\..\TerritoryXml.net");
            StreamWriter stream = new StreamWriter(path);
            XmlSerializer serializer = new XmlSerializer(typeof(List<TerritoryModel>));
            serializer.Serialize(stream, _Territory.ToList());
            stream.Close();
        }

        private void House_Add_Click(object sender, RoutedEventArgs e)
        {
            House win2 = new House(_Houses);
            win2.Show();
        }

        private void Character_Add_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Territory_Add_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
