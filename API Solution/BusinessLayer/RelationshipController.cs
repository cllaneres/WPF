﻿using EntitiesLayer;
using StubDataAccessLayer;
using System;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class RelatinshipController
    {
        DalManager dalMan;

        public RelatinshipController(DalManager dalMan)
        {
            this.dalMan = dalMan;
        }

        public List<Tuple<Character, Character, RelationshipEnum>> getAll()
        {
            return dalMan.Relationship.getAll();
        }

        public List<Tuple<Character, Character, RelationshipEnum>> getById(int id)
        {
            return dalMan.Relationship.getById(id);
        }

        public void update(int id1, int id2, string type)
        {
            dalMan.Relationship.update(id1, id2, type);
        }
        public void delete(int id1, int id2)
        {
            dalMan.Relationship.delete(id1, id2);
        }

    }
}
