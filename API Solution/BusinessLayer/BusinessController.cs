﻿using EntitiesLayer;
using StubDataAccessLayer;
using System.Collections.Generic;

namespace BusinessLayer
{
    public class BusinessController
    {
        DalManager dalMan;
        public CharacterController Character { get; }
        public HouseController House { get; }
        public TerritoryController Territory { get; }
        public RelatinshipController Relationship { get; set; }

        public BusinessController()
        {
            dalMan = DalManager.Instance;
            Character = new CharacterController(dalMan);
            House = new HouseController(dalMan);
            Territory = new TerritoryController(dalMan);
            Relationship = new RelatinshipController(dalMan);
        }
    }
}
