﻿using EntitiesLayer;
using StubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class TerritoryController
    {
        DalManager dalMan;

        public TerritoryController(DalManager dalMan)
        {
            this.dalMan = dalMan;
        }

        public List<Territory> getAll()
        {
            return dalMan.Territory.getAll();
        }

        public Territory getById(int id)
        {
            return dalMan.Territory.getById(id);
        }

        public List<Tuple<int, int>> getAllAssociationById(int id)
        {
            return dalMan.Territory.getAllAssociationById(id);
        }

        public void update(int id, string name, int idhouse, string type)
        {
            dalMan.Territory.update(id, name, idhouse, type);
        }

        public void delete(int id)
        {
            dalMan.Territory.delete(id);
        }
    }
}
