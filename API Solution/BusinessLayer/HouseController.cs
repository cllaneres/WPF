﻿using System.Collections.Generic;
using EntitiesLayer;
using StubDataAccessLayer;
using System;

namespace BusinessLayer
{
    public class HouseController
    {
        DalManager dalMan;

        public HouseController(DalManager dalMan)
        {
            this.dalMan = dalMan;
        }

        public List<House> getAll()
        {
            return dalMan.House.getAll();
        }

        public House getById(int id)
        {
            return dalMan.House.getById(id);
        }

        public void update(int id, string name, int nOU, int leader)
        {
            dalMan.House.update(id, name, nOU, leader);
        }

        public List<Tuple<int, int>> getAllAssociation()
        {
            return dalMan.House.getAllAssociation();
        }
        public List<Tuple<int, int>> getAllAssociationById(int id)
        {
            return dalMan.House.getAllAssociationById(id);
        }

        public List<House> getAllWithTerritoriesAndCharacter()
        {
            return dalMan.House.getAllWithTerritoriesAndCharacter();
        }

        public void delete(int id)
        {
            dalMan.House.delete(id);
        }

    }
}
