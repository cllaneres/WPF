﻿using EntitiesLayer;
using StubDataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class WarController
    {
        House _house1;
        House _house2;
        List<Character> _membersHouse1;
        List<Character> _membersHouse2;
        List<Territory> _territoriesHouse1;
        List<Territory> _territoriesHouse2;
        bool house1Att;
        Fight currentFight;
        public int whoWon { get; set; }//0=personne 1=Attaquant 2=Defenseur 3=Equalite
        private static WarController instance = null;
        private static readonly object padlock = new object();

        public static WarController newInstance(House house1, House house2)
        {
            lock (padlock)
            {
                instance = new WarController(house1, house2);
            }
            return instance;
        }

        public static WarController getInstance()
        {
            return instance;
        }

        public WarController(House house1, House house2)
        {
            BusinessController bc = new BusinessController();
            _house1 = house1;
            _house2 = house2;
            whoWon = 0;
            _membersHouse1 = new List<Character>();
            _membersHouse2 = new List<Character>();
            _territoriesHouse1 = new List<Territory>();
            _territoriesHouse2 = new List<Territory>();


            foreach (int idMembers in bc.House.getAllAssociationById(_house1.Id).Select(t => t.Item1).ToList())
            {
                _membersHouse1.Add(bc.Character.getById(idMembers));
            }
            foreach (int idMembers in bc.House.getAllAssociationById(_house2.Id).Select(t => t.Item1).ToList())
            {
                _membersHouse2.Add(bc.Character.getById(idMembers));
            }


            foreach (int idTerr in bc.Territory.getAllAssociationById(_house1.Id).Select(t => t.Item1).ToList())
            {
                _territoriesHouse1.Add(bc.Territory.getById(idTerr));
            }
            foreach (int idTerr in bc.Territory.getAllAssociationById(_house2.Id).Select(t => t.Item1).ToList())
            {
                _territoriesHouse2.Add(bc.Territory.getById(idTerr));
            }

            house1Att = true;
            currentFight = null;

            if(_membersHouse1.Count==0 || _membersHouse2.Count==0 || _territoriesHouse1.Count==0 || _territoriesHouse2.Count==0 || _house1.NumbersOfUnities<=0 || _house2.NumbersOfUnities <= 0)
            {
                throw new ArgumentException("Une maison n'est pas valide");
            }
        }

        public House houseAtt
        {
            get
            {
                if (house1Att)
                {
                    return _house1;
                }
                else
                {
                    return _house2;
                }
            }
        }

        public List<Territory> territoriesAtt
        {
            get
            {
                if (house1Att)
                {
                    return _territoriesHouse1;
                }
                else
                {
                    return _territoriesHouse2;
                }
            }
        }

        public List<Character> membersAtt
        {
            get
            {
                if (house1Att)
                {
                    return _membersHouse1;
                }
                else
                {
                    return _membersHouse2;
                }
            }
        }

        public House houseDef
        {
            get
            {
                if (house1Att)
                {
                    return _house2;
                }
                else
                {
                    return _house1;
                }
            }
        }

        public List<Territory> territoriesDef
        {
            get
            {
                if (house1Att)
                {
                    return _territoriesHouse2;
                }
                else
                {
                    return _territoriesHouse1;
                }
            }
        }

        public List<Character> membersDef
        {
            get
            {
                if (house1Att)
                {
                    return _membersHouse2;
                }
                else
                {
                    return _membersHouse1;
                }
            }
        }

        public void setCurrentFight(int idTerr, int idHerosAtt, int nbUnitesAtt, int idHerosDef, int nbUnitesDef)
        {
            currentFight = new Fight(houseAtt, houseDef, territoriesDef.Find(x => x.Id == idTerr), membersAtt.Find(x => x.Id == idHerosAtt), nbUnitesAtt, membersDef.Find(x => x.Id == idHerosDef), nbUnitesDef);
        }

        public Fight getCurrentFight()
        {
            return currentFight;
        }

        public int calcFight(int idTerr, int idHerosAtt, int nbUnitesAtt, int idHerosDef, int nbUnitesDef)
        {
            setCurrentFight(idTerr, idHerosAtt, nbUnitesAtt, idHerosDef, nbUnitesDef);
            currentFight.calcFight();

            if (currentFight.herosAtt.Caracteristics.Pv == 0)
            {
                membersAtt.Remove(currentFight.herosAtt);
            }

            if (currentFight.herosDef.Caracteristics.Pv == 0)
            {
                membersDef.Remove(currentFight.herosDef);
            }

            if (membersAtt.Count == 0 || houseAtt.NumbersOfUnities == 0)
            {
                whoWon = 2;
            }
            if (membersDef.Count == 0 || houseDef.NumbersOfUnities == 0)
            {
                whoWon = (whoWon == 2 ? 3 : 1);
            }

            house1Att = !house1Att;

            return whoWon;
        }

    }
}
