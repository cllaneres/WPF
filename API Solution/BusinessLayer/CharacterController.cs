﻿using System.Collections.Generic;
using EntitiesLayer;
using StubDataAccessLayer;

namespace BusinessLayer
{
    public class CharacterController
    {
        DalManager dalMan;

        public CharacterController(DalManager dalMan)
        {
            this.dalMan = dalMan;
        }
        public List<Character> getAll()
        {
            return dalMan.Character.getAll();
        }
        public Character getById(int id)
        {
            return dalMan.Character.getById(id);
        }

        public void update(int id, string firstname, string lastname, int bravoury, int hp, int crazyness, int idhouse, int strength, string type)
        {
            dalMan.Character.update(id, firstname, lastname, bravoury, hp, crazyness, idhouse, strength, type);
        }

        public void delete(int id)
        {
            dalMan.Character.delete(id);
        }

    }
}
