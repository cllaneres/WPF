﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessLayer;
using StubDataAccessLayer;
using System.Collections.Generic;
using EntitiesLayer;

namespace TestUnitaire
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestCreatCharacter()
        {
            DalManager dal = DalManager.Instance;
            CharacterController charac = new CharacterController(dal);
            charac.update(-1, "test", "test", 10, 10, 10, 1, 10, "LEADER");
            List<Character> listChara = charac.getAll();
            Character MyCharaTest = null;
            foreach (Character c in listChara)
            {
                if (c.FirstName == "test" && c.LastName == "test")
                {
                    MyCharaTest = c;
                }
            }
            Assert.IsNotNull(MyCharaTest, "Le Character n'a pas été créé");
        }
        [TestMethod]
        public void TestEditCharacter()
        {
            DalManager dal = DalManager.Instance;
            CharacterController charac = new CharacterController(dal);
            List<Character> listChara = charac.getAll();
            Character MyCharaTest = null;
            foreach (Character c in listChara)
            {
                if (c.FirstName == "test" && c.LastName == "test")
                {
                    MyCharaTest = c;
                }
            }
            charac.update(MyCharaTest.Id, "test1", "test2", 11, 11, 11, 2, 11, "WITCH");
            MyCharaTest = charac.getById(MyCharaTest.Id);
            Assert.AreEqual(MyCharaTest.FirstName, "test1");
        }
        [TestMethod]
        public void TestDeletCharacter()
        {

            DalManager dal = DalManager.Instance;
            CharacterController charac = new CharacterController(dal);
            List<Character> listChara = charac.getAll();
            Character MyCharaTest = null;
            foreach (Character c in listChara)
            {
                if (c.FirstName == "test1" && c.LastName == "test2")
                {
                    MyCharaTest = c;
                }
            }
            charac.delete(MyCharaTest.Id);
            listChara = charac.getAll();
            foreach (Character c in listChara)
            {
                if (c == MyCharaTest)
                {
                    Assert.Fail("character non supprimer");
                }
            }
        }

        [TestMethod]
        public void TestEditRelationShip()
        {
            DalManager dal = DalManager.Instance;
            CharacterController charac1 = new CharacterController(dal);
            CharacterController charac2 = new CharacterController(dal);
            charac1.update(-1, "test10", "test10", 10, 10, 10, 1, 10, "LEADER");
            charac2.update(-1, "test20", "test20", 10, 10, 10, 1, 10, "LEADER");
            List<Character> listChara = charac1.getAll();
            listChara = charac1.getAll();
            Character MyCharaTest1 = null;
            Character MyCharaTest2 = null;
            foreach (Character c in listChara)
            {
                if (c.FirstName == "test10" && c.LastName == "test10")
                {
                    MyCharaTest1 = c;
                }
                if (c.FirstName == "test20" && c.LastName == "test20")
                {
                    MyCharaTest2 = c;
                }
            }
            RelatinshipController relation = new RelatinshipController(dal);
            relation.update(MyCharaTest1.Id, MyCharaTest2.Id, "RIVALRY");
            Assert.AreEqual(MyCharaTest1.Relationships.Values.ToString(), MyCharaTest2.Relationships.Values.ToString());
        }

        [TestMethod]
        public void TestCreateHouse()
        {
            DalManager dal = DalManager.Instance;
            HouseController house = new HouseController(dal);
            house.update(-1, "test", 100, 10);
            List<House> listHouse = house.getAll();
            House myHouseTest = null;
            foreach (House c in listHouse)
            {
                if (c.Name == "test")
                {
                    myHouseTest = c;
                }
            }
            Assert.IsNotNull(myHouseTest, "House n'a pas été créé");
        }

        [TestMethod]
        public void TestEditHouse()
        {
            DalManager dal = DalManager.Instance;
            HouseController house = new HouseController(dal);
            List<House> listHouse = house.getAll();
            House myHouseTest = null;
            foreach (House c in listHouse)
            {
                if (c.Name == "test")
                {
                    myHouseTest = c;
                }
            }
            house.update(myHouseTest.Id, "test", 150, 10);
            myHouseTest = house.getById(myHouseTest.Id);
            Assert.AreEqual(myHouseTest.NumbersOfUnities, 150);


        }

        [TestMethod]
        public void TestDeletHouse()
        {

            DalManager dal = DalManager.Instance;
            HouseController house = new HouseController(dal);
            List<House> listHouse = house.getAll();
            House myHouseTest = null;
            foreach (House c in listHouse)
            {
                if (c.Name == "test")
                {
                    myHouseTest = c;
                }
            }
            house.delete(myHouseTest.Id);
            listHouse = house.getAll();
            foreach (House c in listHouse)
            {
                if (c == myHouseTest)
                {
                    Assert.Fail("house non supprimer");
                }
            }
        }

        [TestMethod]
        public void TestCreateTerritory()
        {
            DalManager dal = DalManager.Instance;
            TerritoryController territory = new TerritoryController(dal);
            territory.update(-1, "test", 1, "SEA");
            List<Territory> listTerritory = territory.getAll();
            Territory myTerritoryTest = null;
            foreach (Territory c in listTerritory)
            {
                if (c.Name == "test")
                {
                    myTerritoryTest = c;
                }
            }
            Assert.IsNotNull(myTerritoryTest, "Territory n'a pas été créé");
        }

        [TestMethod]
        public void TestEditTerritory()
        {
            DalManager dal = DalManager.Instance;
            TerritoryController territory = new TerritoryController(dal);
            List<Territory> listTerritory = territory.getAll();
            Territory myTerritoryTest = null;
            foreach (Territory c in listTerritory)
            {
                if (c.Name == "test")
                {
                    myTerritoryTest = c;
                }
            }
            territory.update(myTerritoryTest.Id, "test", 1, "MOUNTAIN");
            myTerritoryTest = territory.getById(myTerritoryTest.Id);
            Assert.AreEqual(myTerritoryTest.TerritoryType.ToString(), "MOUNTAIN");
        }

        [TestMethod]
        public void TestDeletTerritory()
        {

            DalManager dal = DalManager.Instance;
            TerritoryController territory = new TerritoryController(dal);
            List<Territory> listTerritory = territory.getAll();
            Territory myTerritoryTest = null;
            foreach (Territory c in listTerritory)
            {
                if (c.Name == "test")
                {
                    myTerritoryTest = c;
                }
            }
            territory.delete(myTerritoryTest.Id);
            listTerritory = territory.getAll();
            foreach (Territory c in listTerritory)
            {
                if (c == myTerritoryTest)
                {
                    Assert.Fail("Territory non supprimer");
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InvalidHouse()
        {
            DalManager dal = DalManager.Instance;
            HouseController house1 = new HouseController(dal);
            house1.update(-1, "test1", 100, 10);
            HouseController house2 = new HouseController(dal);
            house2.update(-1, "test2", -50, 10);
            List<House> listHouse = house1.getAll();
            House myHouseTest1 = null;
            House myHouseTest2 = null;
            foreach (House c in listHouse)
            {
                if (c.Name == "test1")
                {
                    myHouseTest1 = c;
                }
                if (c.Name == "test2")
                {
                    myHouseTest2 = c;
                }
            }
            WarController war = WarController.newInstance(myHouseTest2, myHouseTest1);
        }

    }
}