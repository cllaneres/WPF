﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using BusinessLayer;

namespace ThronesTournamentConsole
{
    class Program
    {
        static BusinessController bc = new BusinessController();
        static void Main(string[] args)
        {
            int choix = 0;
            bool veuxQuitter = false;
            
            while (!veuxQuitter)
            {
                Console.Write("\n");
                Console.WriteLine("1 - Creer une maison");
                Console.WriteLine("2 - Creer un personnage");
                Console.WriteLine("3 - Creer une relation");
                Console.WriteLine("4 - Creer un territoire");
                Console.WriteLine("5 - GUERRE A 2 JOUEURS !");
                Console.WriteLine("6 - Quitter");

                while (choix > 6 || choix <= 0)
                {
                    Console.Write("Choix : ");
                    choix = Int32.Parse(Console.ReadLine());
                }

                switch (choix)
                {
                    case 1:
                        creerMaison();
                        break;
                    case 2:
                        creerPerso();
                        break;
                    case 3:
                        creerRelation();
                        break;
                    case 4:
                        creerTerritoire();
                        break;
                    case 5:
                        war2Players();
                        break;
                    default:
                        veuxQuitter = true;
                        break;
                }
                choix = 0;
            }
            
        }

        private static void creerMaison()
        {
            string nom;
            int unites;
            Console.Write("\nNom de la maison : ");
            nom = Console.ReadLine();
            Console.Write("Nombre d'unites : ");
            unites = Int32.Parse(Console.ReadLine());
            
            bc.House.update(-1, nom, unites, -1);
        }

        private static void creerPerso()
        {
            int i = 0;
            int choixMaison=0;
            int choixType = 0;
            int nbType = 0;
            string nom;
            string prenom;
            int pv;
            int bravoure;
            int folie;
            int force;
            bool estLeader;
            List<House> houses = bc.House.getAll();
            if (houses.Count > 0)
            {
                foreach (House h in houses)
                {
                    i++;
                    Console.WriteLine(i + " - " + h.Name);
                }

                while (choixMaison > i || choixMaison <= 0)
                {
                    Console.Write("Maison du nouveau personnage : ");
                    choixMaison = Int32.Parse(Console.ReadLine());
                }
                Console.Write("\nPrenom du personnages : ");
                prenom = Console.ReadLine();
                Console.Write("Nom du personnages : ");
                nom = Console.ReadLine();
                Console.Write("Vie du personnages : ");
                pv = Int32.Parse(Console.ReadLine());
                Console.Write("Bravoure du personnages : ");
                bravoure = Int32.Parse(Console.ReadLine());
                Console.Write("Folie du personnages : ");
                folie = Int32.Parse(Console.ReadLine());
                Console.Write("Force du personnages : ");
                force = Int32.Parse(Console.ReadLine());
                Console.Write("Leader de sa maison ? (1=oui) : ");
                estLeader = (Console.ReadLine() == "1" ? true : false);
                foreach (CharacterTypeEnum t in Enum.GetValues(typeof(CharacterTypeEnum)))
                {
                    nbType++;
                    Console.WriteLine(nbType + " - " + t.ToString());
                }
                while (choixType > nbType || choixType <= 0)
                {
                    Console.Write("Type du personnage : ");
                    choixType = Int32.Parse(Console.ReadLine());
                }
                bc.Character.update(-1, nom, prenom, bravoure, pv, folie, houses[choixMaison - 1].Id, force, ((CharacterTypeEnum)(choixType - 1)).ToString());
                if(estLeader)
                {
                    bc.House.update(houses[choixMaison - 1].Id, houses[choixMaison-1].Name, houses[choixMaison-1].NumbersOfUnities, bc.Character.getAll().Find(t => (t.FirstName==nom && t.LastName==prenom && t.House== houses[choixMaison - 1].Id)).Id);
                }
            }
        }

        private static void creerRelation()
        {
            int nbHouse;
            int nbHousers;
            int nbRelation;
            int[] maisons= { 0, 0 };
            int[] persos = { 0, 0 };
            int relation;
            List<House> houses = bc.House.getAll();
            if (houses.Count > 0)
            {
                for(int i=0;i<=1;i++)
                {
                    Console.WriteLine("---- Personnage "+(i+1)+" ----");
                    nbHouse = 0;
                    foreach (House h in houses)
                    {
                        nbHouse++;
                        Console.WriteLine(nbHouse + " - " + h.Name);
                    }
                    while (maisons[i] > nbHouse || maisons[i] <= 0)
                    {
                        Console.Write("Maison du personnage : ");
                        maisons[i] = Int32.Parse(Console.ReadLine());
                    }
                    nbHousers = 0;
                    foreach (int idMembers in bc.House.getAllAssociationById(houses[maisons[i] - 1].Id).Select(t => t.Item1).ToList())
                    {
                        Character c = bc.Character.getById(idMembers);
                        nbHousers++;
                        Console.WriteLine(nbHousers + " - " + c.FullName);
                    }
                    while (persos[i] > nbHousers || persos[i] <= 0)
                    {
                        Console.Write("Personnage " + (i + 1) + " : ");
                        persos[i] = Int32.Parse(Console.ReadLine());
                    }
                    persos[i] = bc.House.getAllAssociationById(houses[maisons[i] - 1].Id).Select(t => t.Item1).ToList()[persos[i]-1];

                }
                Console.Write("\n");
                nbRelation = 0;
                foreach (RelationshipEnum r in Enum.GetValues(typeof(RelationshipEnum)))
                {
                    nbRelation++;
                    Console.WriteLine(nbRelation + " - " + r.ToString());
                }
                relation = 0;
                while (relation > nbRelation || relation <= 0)
                {
                    Console.Write("\nRelation : ");
                    relation = Int32.Parse(Console.ReadLine());
                }
                bc.Relationship.update(persos[0], persos[1], ((RelationshipEnum)relation-1).ToString());
            }
        }

        private static void creerTerritoire()
        {
            int nbHouse = 0;
            int nbType = 0;
            int choixMaison = 0;
            int choixType = 0;
            string nom;
            List<House> houses = bc.House.getAll();
            if (houses.Count > 0)
            {
                foreach (House h in houses)
                {
                    nbHouse++;
                    Console.WriteLine(nbHouse + " - " + h.Name);
                }
                while (choixMaison > nbHouse || choixMaison <= 0)
                {
                    Console.Write("Maison pour le territoire : ");
                    choixMaison = Int32.Parse(Console.ReadLine());
                }

                Console.Write("\nNom du territoire : ");
                nom = Console.ReadLine();

                foreach (TerritoryType t in Enum.GetValues(typeof(TerritoryType)))
                {
                    nbType++;
                    Console.WriteLine(nbType + " - " + t.ToString());
                }
                while (choixType > nbType || choixType <= 0)
                {
                    Console.Write("Type du territoire : ");
                    choixType = Int32.Parse(Console.ReadLine());
                }
                bc.Territory.update(-1, nom, houses[choixMaison-1].Id, ((TerritoryType)choixType - 1).ToString());
            }
        }

        private static void war2Players()
        {
            //BusinessLayer.WarController 
            WarController wc;
            bool finish = false;
            int nbHouse = 0;
            List<int> maisons = new List<int>();
            List<House> houses = bc.House.getAllWithTerritoriesAndCharacter();
            bool differentHouse = false;

            while(!differentHouse)
            {
                maisons = new List<int>();
                for (int i = 0; i <= 1; i++)
                {
                    nbHouse = 0;
                    maisons.Add(0);
                    foreach (House h in houses)
                    {
                        nbHouse++;
                        Console.WriteLine(nbHouse + " - " + h.Name);
                    }
                    while (maisons[i] > nbHouse || maisons[i] <= 0)
                    {
                        Console.Write("Maison pour le joueur " + (i + 1) + " : ");
                        maisons[i] = Int32.Parse(Console.ReadLine());
                    }
                }
                differentHouse = (maisons[0] != maisons[1]);
            }

            wc = BusinessLayer.WarController.newInstance(bc.House.getById(maisons[0]-1), bc.House.getById(maisons[1]-1));

            while (!finish)
            {
                Console.WriteLine("\n"+wc.houseAtt.Name+" attaque !");
                int nbTer = 0;
                int choixTer = 0;
                int nbCharAtt = 0;
                int choixCharAtt = 0;
                int nbCharDef = 0;
                int choixCharDef = 0;
                int nbUnitAtt;
                int nbUnitDef;
                foreach (Territory ter in wc.territoriesDef)
                {
                    nbTer++;
                    Console.WriteLine(nbTer + " - " + ter.Name+" ("+ter.TerritoryType+")");
                }
                while (choixTer > nbTer || choixTer <= 0)
                {
                    Console.Write("Territoire à attaquer : ");
                    choixTer = Int32.Parse(Console.ReadLine());
                }

                Console.Write("\n");
                foreach (Character hero in wc.membersAtt)
                {
                    nbCharAtt++;
                    Console.WriteLine(nbCharAtt + " - " + hero.FullName + " (" + hero.Caracteristics + ")");
                }
                while (choixCharAtt > nbCharAtt || choixCharAtt <= 0)
                {
                    Console.Write("Héro menant l'attaque : ");
                    choixCharAtt = Int32.Parse(Console.ReadLine());
                }
                Console.Write("Nombre d'unités en attaque ("+wc.houseAtt.NumbersOfUnities+" disponibles) : ");
                nbUnitAtt = Int32.Parse(Console.ReadLine());

                Console.WriteLine("\n"+wc.houseDef.Name + " defend !");
                foreach (Character hero in wc.membersDef)
                {
                    nbCharDef++;
                    Console.WriteLine(nbCharDef + " - " + hero.FullName + " (" + hero.Caracteristics + ")");
                }
                while (choixCharDef > nbCharDef || choixCharDef <= 0)
                {
                    Console.Write("Héro menant la défense : ");
                    choixCharDef = Int32.Parse(Console.ReadLine());
                }
                Console.Write("Nombre d'unités en défense (" + wc.houseDef.NumbersOfUnities + " disponibles) : ");
                nbUnitDef = Int32.Parse(Console.ReadLine());

                wc.calcFight(wc.territoriesDef[choixTer - 1].Id, wc.membersAtt[choixCharAtt - 1].Id, nbUnitAtt, wc.membersDef[choixCharDef - 1].Id, nbUnitDef);

                Fight fight = wc.getCurrentFight();
                Console.WriteLine("\n-----RESULTAT DE LA BATAILLE-----");
                if(fight.herosAtt.Caracteristics.Pv!=0)
                {
                    Console.WriteLine("Vie du heros attaquant " + fight.herosAtt.Caracteristics.Pv);
                }
                else
                {
                    Console.WriteLine("LE HERO ATTAQUANT EST MORT AU COMBAT !");
                }
                Console.WriteLine("Unites survivantes en attaque : " + fight.nbUnitesAtt);
                if (fight.herosDef.Caracteristics.Pv != 0)
                {
                    Console.WriteLine("Vie du heros defenseur " + fight.herosDef.Caracteristics.Pv);
                }
                else
                {
                    Console.WriteLine("LE HERO DEFENSEUR EST MORT AU COMBAT !");
                }
                Console.WriteLine("Unites survivantes en defense : " + fight.nbUnitesDef);
                finish = true;
                switch (wc.whoWon)
                {
                    case 0:
                        Console.Write("Continuer la partie ? (STOP=arreter) : ");
                        finish = (Console.ReadLine() == "STOP" ? true : false);
                        break;
                    case 1:
                        Console.WriteLine("L'ATTAQUANT A GAGNE !!!");
                        break;
                    case 2:
                        Console.WriteLine("LE DEFENSEUR A GAGNE !!!");
                        break;
                    case 3:
                        Console.WriteLine("EQUALITE");
                        break;
                }
                
            }
        }

    }
}
