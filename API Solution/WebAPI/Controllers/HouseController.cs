﻿using BusinessLayer;
using EntitiesLayer;
using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class HouseController : ApiController
    {
        public List<HouseDTO> GetAllHouses()
        {
            List<HouseDTO> list = new List<HouseDTO>();
            BusinessController bc = new BusinessController();

            foreach (var house in bc.House.getAll())
            {
                list.Add(new HouseDTO(house));
            }

            return list;
        }

        public HouseDTO GetHouse(int id)
        {
            BusinessController bc = new BusinessController();
            House hou = bc.House.getById(id);
            if(hou != null)
                return new HouseDTO(hou);
            return null;
        }

        [Route("api/house/terrAndChar")]
        public List<HouseDTO> GetAllHousesWithTerritoriesAndCharacters()
        {
            List<HouseDTO> list = new List<HouseDTO>();
            BusinessController bc = new BusinessController();

            foreach (var house in bc.House.getAllWithTerritoriesAndCharacter())
            {
                list.Add(new HouseDTO(house));
            }

            return list;
        }

        [Route("api/house/modify")]
        [HttpPost]
        public void ModifyHouse(HouseDTO house)
        {
            if (house != null)
            {
                BusinessController bc = new BusinessController();
                bc.House.update(house.Id, house.Name, house.NumbersOfUnities, house.Leader);
            }
        }

        [Route("api/house/delete/{id}")]
        [HttpGet]
        public void DeleteHouse(int id)
        {
            BusinessController bc = new BusinessController();
            bc.House.delete(id);
        }
    }
}
