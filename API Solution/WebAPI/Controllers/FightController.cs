﻿using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;
using BusinessLayer;
using EntitiesLayer;
using System;

namespace WebAPI.Controllers
{
    public class FightController : ApiController
    {
        public static int futureTerritory;
        [Route("api/fight/{idTerrain}/{idHerosAtt}/{nbUnitesAtt}/{idHerosDef}/{nbUnitesDef}")]
        [HttpGet]
        public FightDTO calcFight(int idTerrain, int idHerosAtt, int nbUnitesAtt, int idHerosDef, int nbUnitesDef)
        {
            BusinessLayer.WarController wc = BusinessLayer.WarController.getInstance();

            wc.calcFight(idTerrain, idHerosAtt, nbUnitesAtt, idHerosDef, nbUnitesDef);

            FightDTO res = new FightDTO(wc.getCurrentFight());

            return res;
        }

        [Route("api/fight/getTerritory")]
        [HttpGet]
        public TerritoryDTO getTerritoryAdvSolo()
        {
            BusinessLayer.WarController wc = BusinessLayer.WarController.getInstance();

            Random rnd = new Random();
            int posTerritoriesAtt = rnd.Next(wc.territoriesDef.Count);
            futureTerritory = wc.territoriesDef[posTerritoriesAtt].Id;
            return new TerritoryDTO(wc.territoriesDef[posTerritoriesAtt]);
        }

        [Route("api/fight/{idTerrain}/{idHerosAtt}/{nbUnitesAtt}")]
        [HttpGet]
        public FightDTO calcFightAttSolo(int idTerrain, int idHerosAtt, int nbUnitesAtt)
        {
            BusinessLayer.WarController wc = BusinessLayer.WarController.getInstance();

            Random rnd = new Random();
            int posHeroDef = rnd.Next(wc.membersDef.Count);
            rnd = new Random();
            int nbUnitesDef = rnd.Next(1, wc.houseDef.NumbersOfUnities / 2);
            wc.calcFight(idTerrain, idHerosAtt, nbUnitesAtt, wc.membersDef[posHeroDef].Id, nbUnitesDef);

            FightDTO res = new FightDTO(wc.getCurrentFight());

            return res;
        }


        [Route("api/fight/{idHerosDef}/{nbUnitesDef}")]
        [HttpGet]
        public FightDTO calcFightDefSolo(int idHerosDef, int nbUnitesDef)
        {
            BusinessLayer.WarController wc = BusinessLayer.WarController.getInstance();

            Random rnd = new Random();
            int posHeroAtt = rnd.Next(wc.membersAtt.Count);
            rnd = new Random();
            int nbUnitesAtt = rnd.Next(1, wc.houseAtt.NumbersOfUnities / 2);
            wc.calcFight(futureTerritory, wc.membersAtt[posHeroAtt].Id, nbUnitesAtt, idHerosDef, nbUnitesDef);

            FightDTO res = new FightDTO(wc.getCurrentFight());

            return res;
        }
    }
}
