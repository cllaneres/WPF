﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class CharHouseAssocController : ApiController
    {
        public List<CharHouseAssocDTO> GetAllAssociation()
        {
            List<CharHouseAssocDTO> list = new List<CharHouseAssocDTO>();
            BusinessController bc = new BusinessController();

            foreach (Tuple<int, int> charac in bc.House.getAllAssociation())
            {
                list.Add(new CharHouseAssocDTO(charac));
            }

            return list;
        }

        public List<CharHouseAssocDTO> GetAssociationByHouse(int id)
        {
            List<CharHouseAssocDTO> list = new List<CharHouseAssocDTO>();
            BusinessController bc = new BusinessController();
            foreach (Tuple<int, int> charac in bc.House.getAllAssociationById(id))
            {
                list.Add(new CharHouseAssocDTO(charac));
            }
            return list;
        }
    }
}
