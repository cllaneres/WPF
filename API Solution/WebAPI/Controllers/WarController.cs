﻿using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;
using BusinessLayer;
using EntitiesLayer;
using System;

namespace WebAPI.Controllers
{
    public class WarController : ApiController
    {
        [Route("api/war/new/{idHouse1}/{idHouse2}")]
        [HttpGet]
        public WarDTO newWar(int idHouse1, int idHouse2)
        {
            BusinessController bc = new BusinessController();
            BusinessLayer.WarController wc = BusinessLayer.WarController.newInstance(bc.House.getById(idHouse1), bc.House.getById(idHouse2));
            return new WarDTO(wc);
        }

        [Route("api/war/new/{idHouse}/")]
        [HttpGet]
        public WarDTO newWarSolo(int idHouse)
        {
            BusinessController bc = new BusinessController();
            List<House> allHouses = bc.House.getAllWithTerritoriesAndCharacter();
            Random rnd = new Random();
            int posHouseAdv = -1;
            while (posHouseAdv < 0 || allHouses[posHouseAdv].Id == idHouse)
            {
                posHouseAdv = rnd.Next(allHouses.Count);
            }
            BusinessLayer.WarController wc = BusinessLayer.WarController.newInstance(bc.House.getById(idHouse), allHouses[posHouseAdv]);
            return new WarDTO(wc);
        }

        [Route("api/war/current")]
        [HttpGet]
        public WarDTO newWar()
        {
            BusinessLayer.WarController wc = BusinessLayer.WarController.getInstance();
            return new WarDTO(wc);
        }

    }
}
