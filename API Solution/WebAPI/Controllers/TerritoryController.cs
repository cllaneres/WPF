﻿using BusinessLayer;
using EntitiesLayer;
using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class TerritoryController : ApiController
    {
        public List<TerritoryDTO> GetAllTerritories()
        {
            List<TerritoryDTO> list = new List<TerritoryDTO>();
            BusinessController bc = new BusinessController();

            foreach (var charac in bc.Territory.getAll())
            {
                list.Add(new TerritoryDTO(charac));
            }

            return list;
        }

        public TerritoryDTO GetTerritory(int id)
        {
            BusinessController bc = new BusinessController();
            Territory ter = bc.Territory.getById(id);
            if (ter != null)
                return new TerritoryDTO(ter);
            return null;
        }

        [Route("api/territory/modify")]
        [HttpPost]
        public void ModifyTerritory(TerritoryDTO ter)
        {
            if (ter != null)
            {
                BusinessController bc = new BusinessController();
                bc.Territory.update(ter.Id, ter.Name, ter.Owner, ter.TerritoryType);
            }
        }

        [Route("api/territory/delete/{id}")]
        [HttpGet]
        public void DeleteTerritory(int id)
        {
            BusinessController bc = new BusinessController();
            bc.Territory.delete(id);
        }
    }
}
