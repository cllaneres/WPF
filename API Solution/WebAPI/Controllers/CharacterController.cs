﻿using BusinessLayer;
using EntitiesLayer;
using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class CharacterController : ApiController
    {
        public List<CharacterDTO> GetAllCharacters()
        {
            List<CharacterDTO> list = new List<CharacterDTO>();
            BusinessController bc = new BusinessController();

            foreach(Character charac in bc.Character.getAll())
            {
                list.Add(new CharacterDTO(charac));
            }

            return list;
        }

        public CharacterDTO GetCharacter(int id)
        {
            BusinessController bc = new BusinessController();
            Character chara = bc.Character.getById(id);
            if (chara != null)
                return new CharacterDTO(chara);
            return null;
        }

        [Route("api/character/modify")]
        [HttpPost]
        public void ModifyCharacter(CharacterDTO chara)
        {
            if (chara != null)
            {
                BusinessController bc = new BusinessController();
                bc.Character.update(chara.Id, chara.FirstName, chara.LastName, chara.Bravoury, chara.PV, chara.Crazyness, chara.House, chara.Strength, chara.Type);
            }
        }

        [Route("api/character/delete/{id}")]
        [HttpGet]
        public void DeleteCharacter(int id)
        {
            BusinessController bc = new BusinessController();
            bc.Character.delete(id);
        }
    }
}
