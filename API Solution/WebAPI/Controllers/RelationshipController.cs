﻿using BusinessLayer;
using EntitiesLayer;
using System;
using System.Collections.Generic;
using System.Web.Http;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class RelationshipController : ApiController
    {
        public List<RelationshipDTO> GetAllRelationships()
        {

            List<RelationshipDTO> list = new List<RelationshipDTO>();
            BusinessController bc = new BusinessController();
            
            foreach (Tuple<Character, Character, RelationshipEnum> rel in bc.Relationship.getAll())
            {
                list.Add(new RelationshipDTO(rel));
            }

            return list;
        }

        public List<RelationshipDTO> GetRelationshipById(int id)
        {
            List<RelationshipDTO> list = new List<RelationshipDTO>();
            BusinessController bc = new BusinessController();

            foreach (Tuple<Character, Character, RelationshipEnum> rel in bc.Relationship.getById(id))
            {
                list.Add(new RelationshipDTO(rel));
            }

            return list;
        }

        [Route("api/relationship/modify")]
        [HttpPost]
        public void ModifyRelationship(RelationshipDTO rl)
        {
            if (rl != null)
            {
                BusinessController bc = new BusinessController();
                bc.Relationship.update(rl.Char1, rl.Char2, rl.Type);
            }
        }

        [Route("api/character/delete/{id1}/{id2}")]
        [HttpGet]
        public void DeleteCharacter(int id1, int id2)
        {
            BusinessController bc = new BusinessController();
            bc.Relationship.delete(id1, id2);
        }
    }
}