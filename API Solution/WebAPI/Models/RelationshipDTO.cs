﻿using EntitiesLayer;
using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class RelationshipDTO
    {
        public int Char1 { get; set; }
        public int Char2 { get; set; }
        public string Type { get; set; }

        public RelationshipDTO()
        {
            
        }
        public RelationshipDTO(Tuple<Character, Character, RelationshipEnum> rel)
        {
            Char1 = rel.Item1.Id;
            Char2 = rel.Item2.Id;
            Type = rel.Item3.ToString();
        }
    }
}