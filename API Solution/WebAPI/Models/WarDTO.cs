﻿using BusinessLayer;
using EntitiesLayer;
using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class WarDTO
    {
        public HouseDTO houseAtt { get; set; }
        public HouseDTO houseDef { get; set; }
        public List<TerritoryDTO> territoriesAtt { get; set; }
        public List<TerritoryDTO> territoriesDef { get; set; }
        public List<CharacterDTO> heroesAtt { get; set; }
        public List<CharacterDTO> heroesDef { get; set; }
        public int whoWon;

        public WarDTO()
        {

        }
        public WarDTO(WarController wc)
        {
            houseAtt = new HouseDTO(wc.houseAtt);
            houseDef = new HouseDTO(wc.houseDef);
            whoWon = wc.whoWon;
            territoriesAtt = new List<TerritoryDTO>();
            territoriesDef = new List<TerritoryDTO>();
            heroesAtt = new List<CharacterDTO>();
            heroesDef = new List<CharacterDTO>();
            foreach (Territory ter in wc.territoriesAtt)
            {
                territoriesAtt.Add(new TerritoryDTO(ter));
            }
            foreach (Territory ter in wc.territoriesDef)
            {
                territoriesDef.Add(new TerritoryDTO(ter));
            }
            foreach (Character hero in wc.membersAtt)
            {
                heroesAtt.Add(new CharacterDTO(hero));
            }
            foreach (Character hero in wc.membersDef)
            {
                heroesDef.Add(new CharacterDTO(hero));
            }
        }
    }
}
