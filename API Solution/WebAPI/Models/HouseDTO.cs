﻿using EntitiesLayer;

namespace WebAPI.Models
{
    public class HouseDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Leader { get; set; }
        public int NumbersOfUnities { get; set; }

        public HouseDTO()
        {

        }

        public HouseDTO(House hou)
        {
            Id = hou.Id;
            Name = hou.Name;
            if(hou.leader != null)
                Leader = hou.leader.Id;
            else
                Leader = -1;
            NumbersOfUnities = hou.NumbersOfUnities;
        }
    }
}