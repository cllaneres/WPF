﻿using EntitiesLayer;
using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class CharacterDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public int PV { get; set; }
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }

        public int Strength { get; set; }

        public string Type { get; set; }
        public int House { get; set; }

        public CharacterDTO()
        {

        }
        public CharacterDTO(Character chara)
        {
            Id = chara.Id;
            FirstName = chara.FirstName;
            LastName = chara.LastName;
            PV = chara.Caracteristics.Pv;
            Bravoury = chara.Caracteristics.Bravoury;
            Crazyness = chara.Caracteristics.Crazyness;
            Strength = chara.Caracteristics.Strength;
            Type = chara.Caracteristics.Type.ToString();
            House = chara.House;
        }
    }
}