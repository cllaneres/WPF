﻿using EntitiesLayer;

namespace WebAPI.Models
{
    public class TerritoryDTO
    {
        public int Id { get; set;  }
        public string TerritoryType { get; set; }
        public int Owner { get; set; }
        public string Name { get; set; }

        public TerritoryDTO()
        {

        }

        public TerritoryDTO(Territory ter)
        {
            Id = ter.Id;
            TerritoryType = ter.TerritoryType.ToString();
            Name = ter.Name;
            if (ter.Owner != null)
                Owner = ter.Owner.Id;
            else
                Owner = -1;
        }
    }
}