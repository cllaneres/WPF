﻿using EntitiesLayer;
using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class FightDTO
    {
        public HouseDTO houseAtt { get; set; }
        public HouseDTO houseDef { get; set; }
        public TerritoryDTO territory { get; set; }
        public CharacterDTO heroAtt { get; set; }
        public CharacterDTO heroDef { get; set; }

        public FightDTO()
        {

        }
        public FightDTO(Fight fi)
        {
            houseAtt = new HouseDTO(fi.houseAtt);
            houseDef = new HouseDTO(fi.houseDef);
            territory = new TerritoryDTO(fi.territory);
            heroAtt = new CharacterDTO(fi.herosAtt);
            heroDef = new CharacterDTO(fi.herosDef);
        }
    }
}
