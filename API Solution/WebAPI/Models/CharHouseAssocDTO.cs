﻿using EntitiesLayer;
using System;
using System.Collections.Generic;

namespace WebAPI.Models
{
    public class CharHouseAssocDTO
    {
        public int IdChar { get; set; }
        public int IdHouse { get; set; }

        public CharHouseAssocDTO()
        {

        }
        public CharHouseAssocDTO(Tuple<int, int> id)
        {
            IdChar = id.Item1;
            IdHouse = id.Item2;
        }
    }
}