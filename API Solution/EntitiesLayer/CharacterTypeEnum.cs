﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public enum CharacterTypeEnum
    {
        VILLAGER,
        WARRIOR,
        WITCH,
        TACTICIAN,
        LEADER,
        LOSER
    }
}
