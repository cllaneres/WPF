﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public class Fight : EntityObject
    {
        public House houseAtt { get; set; }
        public House houseDef { get; set; }
        public Territory territory { get; set; }
        public Character herosAtt { get; set; }
        public Character herosDef { get; set; }
        public int nbUnitesAtt { get; set; }
        public int nbUnitesDef { get; set; }

        public Fight(House house1, House house2, Territory territory, Character herosAtt, int nbUnitesAtt, Character herosDef, int nbUnitesDef)
        {
            this.houseAtt = house1;
            this.houseDef = house2;
            this.territory = territory;
            this.herosAtt = herosAtt;
            this.herosDef = herosDef;
            this.nbUnitesAtt = nbUnitesAtt;
            this.nbUnitesDef = nbUnitesDef;
        }

        public void calcFight()
        {
            CharacterStats attaquant = new CharacterStats(herosAtt, true, nbUnitesAtt);
            CharacterStats defenseur = new CharacterStats(herosDef, true, nbUnitesDef);

            attaquant.ChangeStatsType(defenseur);
            defenseur.ChangeStatsType(attaquant);

            attaquant.ChangeStatsTerritory(territory);
            defenseur.ChangeStatsTerritory(territory);

            attaquant.ChangeStatsCara(defenseur);
            defenseur.ChangeStatsCara(attaquant);

            CharacterStats.ChangeStatsRelation(attaquant, defenseur);
            CharacterStats.ChangeStatsDifNbUnities(attaquant, defenseur);

            attaquant.calcDead();
            defenseur.calcDead();

            houseAtt.NumbersOfUnities -= (nbUnitesAtt - attaquant.nbUnites);
            houseDef.NumbersOfUnities -= (nbUnitesDef - defenseur.nbUnites);

            herosAtt.Caracteristics.Pv = (int)(herosAtt.Caracteristics.Pv * (double)attaquant.nbUnites / nbUnitesAtt);
            herosDef.Caracteristics.Pv = (int)(herosDef.Caracteristics.Pv * (double)defenseur.nbUnites / nbUnitesDef);

            nbUnitesAtt = attaquant.nbUnites;
            nbUnitesDef = defenseur.nbUnites;
        }

    }
}
