﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public enum TerritoryType
    {
        SEA,
        MOUNTAIN,
        LAND,
        DESERT
    }
}
