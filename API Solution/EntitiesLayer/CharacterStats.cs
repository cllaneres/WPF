﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
namespace EntitiesLayer
{
    class CharacterStats
    {
        Character hero;
        public int crazyness { get; set; }
        public int strength { get; set; }
        public int bravoury { get; set; }
        public double survie { get; set; }
        /*  = 1 si attaquant, 0 si defenseur */
        public bool estAttaquant { get; set; }
        public int nbUnites { get; set; }

        static double survieBaseAtt = 50;
        static double survieBaseDef = 60;
        static double malusStrength = 1;
        static double bonusBravoury = 1;
        static double malusCrazyness = 0.5;
        static double malusSeaDef = 1 / 3;
        static double bonusSeaCrazyness = 1 / 4;
        static double malusMountainAtt = 1 / 2;
        static double bonusMountainSurvDef = 1 / 5;
        static double malusDesertFolie = 0.2;
        static int malusDesertFolieMin = 2;
        static double bonusDesertUniteDef = 1 / 3;

        public CharacterStats(Character chara, bool estAtt, int nbUn)
        {
            hero = chara;
            crazyness = chara.Caracteristics.Crazyness;
            strength = chara.Caracteristics.Strength;
            bravoury = chara.Caracteristics.Bravoury;
            estAttaquant = estAtt;
            nbUnites = nbUn;
            survie = (estAttaquant ? survieBaseAtt : survieBaseDef);
        }

        public void ChangeStatsType(CharacterStats adversaire)
        {
            switch (hero.Caracteristics.Type)
            {
                case CharacterTypeEnum.LEADER:
                    survie *= 1.1;
                    adversaire.survie *= 0.9;
                    break;
                case CharacterTypeEnum.LOSER:
                    adversaire.survie *= 1.1;
                    survie *= 0.9;
                    break;
                case CharacterTypeEnum.TACTICIAN:
                    bravoury += 10;
                    strength -= 10;
                    crazyness -= 10;
                    break;
                case CharacterTypeEnum.WITCH:
                    strength += 10;
                    survie *= 0.9;
                    break;
                case CharacterTypeEnum.WARRIOR:
                    strength += 10;
                    crazyness += 10;
                    break;
                case CharacterTypeEnum.VILLAGER:
                    bravoury += 10;
                    survie *= 0.9;
                    break;
            }
        }


        public void ChangeStatsTerritory(Territory territoire)
        {
            if (estAttaquant)
            {
                ChangeStatsTerritoryAtt(territoire);
            }
            else
            {
                ChangeStatsTerritoryDef(territoire);
            }
        }

        private void ChangeStatsTerritoryDef(Territory territoire)
        {
            switch (territoire.TerritoryType)
            {
                case TerritoryType.SEA:
                    survie = survie + malusSeaDef * survie;
                    break;
                case TerritoryType.DESERT:
                    nbUnites = nbUnites - (int)(nbUnites * bonusDesertUniteDef);
                    break;
            }
        }

        private void ChangeStatsTerritoryAtt(Territory territoire)
        {
            switch (territoire.TerritoryType)
            {
                case TerritoryType.SEA:
                    crazyness = crazyness - (int)(crazyness * bonusSeaCrazyness);
                    break;
                case TerritoryType.MOUNTAIN:
                    strength = (int)(strength * malusMountainAtt);
                    survie = survie + survie * bonusMountainSurvDef;
                    break;
                case TerritoryType.DESERT:
                    crazyness = (int)(crazyness * malusDesertFolie);
                    crazyness = Math.Max(crazyness, malusDesertFolieMin);
                    break;
            }
        }

        public void ChangeStatsCara(CharacterStats adversaire)
        {
            survie += bonusBravoury * bravoury;
            adversaire.survie -= malusStrength * strength;
            survie -= malusCrazyness * crazyness;
            adversaire.survie += malusCrazyness * crazyness;
        }
        public static void ChangeStatsRelation(CharacterStats attaquant, CharacterStats defenseur)
        {
            if (attaquant.hero.Relationships.Keys.Contains(attaquant.hero))
            {
                RelationshipEnum relation = attaquant.hero.Relationships[attaquant.hero];
                int difSurvie = 0;
                switch (relation)
                {
                    case RelationshipEnum.HATRED:
                        difSurvie = -20;
                        break;
                    case RelationshipEnum.RIVALRY:
                        difSurvie = -10;
                        break;
                    case RelationshipEnum.FRIENDSHIP:
                        difSurvie = 10;
                        break;
                    case RelationshipEnum.LOVE:
                        difSurvie = 20;
                        break;
                }
                attaquant.survie += difSurvie;
                defenseur.survie += difSurvie;
            }
        }

        public static void ChangeStatsDifNbUnities(CharacterStats attaquant, CharacterStats defenseur)
        {
            if (attaquant.nbUnites > 1.2 * defenseur.nbUnites)
            {
                attaquant.survie += Math.Min((attaquant.nbUnites - 1.2 * defenseur.nbUnites) / 4, 100 - attaquant.survie);
                defenseur.survie -= Math.Min((attaquant.nbUnites - 1.2 * defenseur.nbUnites) / 6, defenseur.survie);
            }
            else if (defenseur.nbUnites > 1.2 * attaquant.nbUnites)
            {
                defenseur.survie += Math.Min((defenseur.nbUnites - 1.2 * attaquant.nbUnites) / 4, 100);
                attaquant.survie -= Math.Min((defenseur.nbUnites - 1.2 * attaquant.nbUnites) / 6, attaquant.survie);
            }
        }

        public void calcDead()
        {
            Random rnd = new Random();

            for (int i = 0; i < nbUnites; ++i)
            {
                int die = rnd.Next(1, 100);
                if (die > survie)
                {
                    nbUnites--;
                }
            }
        }

    }
}
