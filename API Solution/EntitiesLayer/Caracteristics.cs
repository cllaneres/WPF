﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesLayer
{
    public class Caracteristics
    {
        public int Bravoury { get; set; }
        public int Crazyness { get; set; }
        public int Strength { get; set; }
        public int Pv { get; set; }
        public CharacterTypeEnum Type { get; set; }

        public Caracteristics()
        {
            Pv = 100;
            Bravoury = 50;
            Crazyness = 50;
            Strength = 50;
            Type = CharacterTypeEnum.VILLAGER;
        }
        public Caracteristics(int pv)
        {
            Pv = pv;
            Bravoury = 50;
            Crazyness = 50;
            Strength = 50;
            Type = CharacterTypeEnum.VILLAGER;
        }
        public Caracteristics(int pv, int bravoury, int crazyness, int strength, CharacterTypeEnum type)
        {
            Pv = pv;
            Bravoury = bravoury;
            Crazyness = crazyness;
            Strength = strength;
            Type = type;
        }

        public override string ToString()
        {
            return "[P:" + Pv + "/B:" + Bravoury + "/C:" + Crazyness + "/S:" + Strength + "]";
        }
    }
}
