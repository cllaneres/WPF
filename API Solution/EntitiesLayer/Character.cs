﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public class Character : EntityObject
    {
        public int Id { get; set;  }
        public Caracteristics Caracteristics { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public int House { get; set; }
        public Dictionary<Character, RelationshipEnum> Relationships { get; set; }

        public String FullName
        {
            get { return FirstName + " " + LastName; }
        }
        
        public Character(int Id, String firstName, String lastName, int pv)
        {
            this.Id = Id;
            Caracteristics = new Caracteristics(pv);
            Relationships = new Dictionary<Character, RelationshipEnum>();
            FirstName = firstName;
            LastName = lastName;
        }
        public Character(int Id, String firstName, String lastName, Caracteristics caracteristics, int house)
        {
            this.Id = Id;
            Caracteristics = caracteristics;
            Relationships = new Dictionary<Character, RelationshipEnum>();
            FirstName = firstName;
            LastName = lastName;
            House = house;
        }

        public void AddRelation( Character character, RelationshipEnum relationshipEnum)
        {
            Relationships.Add(character, relationshipEnum);
        }

        override public String ToString()
        {
            return FirstName + " " + LastName + " " + Caracteristics;
        }
        /// TODO : Methodes !
    }
}
