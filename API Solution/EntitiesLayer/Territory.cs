﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public class Territory : EntityObject
    {
        public int Id { get; set; }
        public TerritoryType TerritoryType { get; set; }
        public House Owner { get; set; }
        public string Name{ get; set; }

        public Territory(TerritoryType territoryType)
        {
            TerritoryType = territoryType;
        }

        public Territory(int Id, TerritoryType territoryType,House owner, string name)
        {
            this.Id = Id;
            TerritoryType = territoryType;
            Owner = owner;
            Name = name;
        }
    }
}
