﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public class House : EntityObject
    {
        //public List<Character> Housers { get; set; }
        public List<int> HousersId { get; set; }

        public int Id { get; set; }

        public Character leader { get; set; }

        public String Name { get; set; }
        public int NumbersOfUnities { get; set; }

        public House(String name, int nbUnit)
        {
            NumbersOfUnities = nbUnit;
            //Housers = new List<Character>();
            Name = name;
        }

        public House(String name)
        {
            NumbersOfUnities = 0;
            //Housers = new List<Character>();
            Name = name;
        }
        public House(int Id, String name, Character leader, int NumberOfUnities)
        {
            this.Id = Id;
            NumbersOfUnities = NumberOfUnities;
            this.leader = leader;
            Name = name;
        }
        /*
        public void AddHousers(Character character)
        {
            Housers.Add(character);
        }
        override public String ToString()
        {
            String ret = Name + ". Nb Units :  " + NumbersOfUnities;
            foreach(Character houser in Housers)
            {
                ret += "\n" + houser;
            }
            return ret;
        }
        */
    }
}
