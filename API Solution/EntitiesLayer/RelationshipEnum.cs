﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntitiesLayer
{
    public enum RelationshipEnum
    {
        FRIENDSHIP,
        LOVE,
        HATRED,
        RIVALRY
    }
}
