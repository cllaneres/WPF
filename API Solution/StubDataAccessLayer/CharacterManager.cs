﻿using System;
using System.Collections.Generic;
using EntitiesLayer;
using System.Data;

namespace StubDataAccessLayer
{
    public class CharacterManager
    {

        //IDal dal;
        public CharacterManager()
        {
            //dal = DalManager.Instance.dal;
        }

        public List<Character> getAll()
        {
            List<Character> characters = new List<Character>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Character");

            foreach (DataRow elem in data.Rows)
            {
                Character character = new Character((int)elem["Id"], (string)elem["firstName"], (string)elem["lastName"], new Caracteristics((int)elem["Hp"], (int)elem["Bravoury"], (int)elem["Crazyness"], (int)elem["Strength"], (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), (string)elem["type"])), (int)elem["IdHouse"]);

                characters.Add(character);
            }

            return characters;
        }

        public Character getById(int id)
        {
            Character character = null;
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Character WHERE ID=" + id);

            foreach (DataRow elem in data.Rows)
            {
                character = new Character((int)elem["Id"], (string)elem["firstName"], (string)elem["lastName"], new Caracteristics((int)elem["Hp"], (int)elem["Bravoury"], (int)elem["Crazyness"], (int)elem["Strength"], (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), (string)elem["type"])), (int)elem["IdHouse"]);
            }

            return character;
        }

        public List<Character> getByHouseId(int id)
        {
            List<Character> characters = new List<Character>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Character WHERE idHouse=" + id);

            foreach (DataRow elem in data.Rows)
            {
                Character character = new Character((int)elem["Id"], (string)elem["firstName"], (string)elem["lastName"], new Caracteristics((int)elem["Hp"], (int)elem["Bravoury"], (int)elem["Crazyness"], (int)elem["Strength"], (CharacterTypeEnum)Enum.Parse(typeof(CharacterTypeEnum), (string)elem["type"])), (int)elem["IdHouse"]);

                characters.Add(character);
            }

            return characters;
        }

        public void update(int id, string firstname, string lastname, int bravoury, int hp, int crazyness, int idhouse, int strength, string type)
        {
            string request;
            DataTable dt;
            request = "SELECT Id, firstName, lastName, Bravoury, Hp, Crazyness, idHouse, Strength, type FROM Character WHERE Id=" + id;

            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if (id != -1 && dt.Rows.Count > 0)
            {
                dt.Rows[0]["firstname"] = firstname;
                dt.Rows[0]["lastName"] = lastname;
                dt.Rows[0]["Bravoury"] = bravoury;
                dt.Rows[0]["Hp"] = hp;
                dt.Rows[0]["Crazyness"] = crazyness;
                dt.Rows[0]["idHouse"] = idhouse;
                dt.Rows[0]["Strength"] = strength;
                dt.Rows[0]["type"] = type;
            }
            else
            {
                string request_tmp = "SELECT MAX(Id) as idm FROM Character";
                DataTable dt_tmp = DalManager.Instance.dal.selectByDataAdapter(request_tmp);
                id = ((int)dt_tmp.Rows[0]["idm"]) + 1;

                request = "SELECT Id, firstName, lastName, Bravoury, Hp, Crazyness, idHouse, Strength, type FROM Character";
                object[] obj = { id, firstname, lastname, bravoury, hp, crazyness, idhouse, strength, type };
                dt.Rows.Add(obj);
            }
            DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
        }
        public void delete(int id)
        {
            string request;
            DataTable dt;
            request = "SELECT Id, firstName, lastName, Bravoury, Hp, Crazyness, idHouse, Strength, type FROM Character WHERE Id=" + id;

            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if (dt.Rows.Count > 0)
            {
                // Delete les relationship
                List<Tuple<Character, Character, RelationshipEnum>> relations = DalManager.Instance.Relationship.getById(id);
                foreach (Tuple<Character, Character, RelationshipEnum> rel in relations)
                {
                    DalManager.Instance.Relationship.delete(rel.Item1.Id, rel.Item2.Id);
                }

                dt.Rows[0].Delete();
                DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
            }
        }

    }

}