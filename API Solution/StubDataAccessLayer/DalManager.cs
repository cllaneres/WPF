﻿using System.Data;

namespace StubDataAccessLayer
{
    public class DalManager
    {
        public IDal dal { get; }
        private static DalManager _instance = null;
        private static readonly object padlock = new object();

        public CharacterManager Character { get; }
        public HouseManager House { get; }
        public RelationshipManager Relationship { get; }
        public TerritoryManager Territory { get; }

        public DalManager()
        {
            dal = new SQLServerDal();
            Character = new CharacterManager();
            House = new HouseManager();
            Relationship = new RelationshipManager();
            Territory = new TerritoryManager();
        }

        public static DalManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (padlock)
                    {
                        if (_instance == null)
                        {
                            _instance = new DalManager();
                        }
                    }
                }
                return _instance;
            }
        }

    }

}
