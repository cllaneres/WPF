﻿using System;
using System.Collections.Generic;
using EntitiesLayer;
using System.Data;

namespace StubDataAccessLayer
{
    public class TerritoryManager
    {
        public TerritoryManager()
        {
        }

        public List<Territory> getAll()
        {
            List<Territory> territories = new List<Territory>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Territory");

            foreach (DataRow elem in data.Rows)
            {
                int idHouse = (int)elem["idHouse"];
                House owner = DalManager.Instance.House.getById(idHouse);
                Territory territory = new Territory((int)elem["Id"], (TerritoryType)Enum.Parse(typeof(TerritoryType), (string)elem["Type"]), owner, (string)elem["name"]);
                territories.Add(territory);
            }

            return territories;
        }

        public Territory getById(int id)
        {
            Territory territory = null;
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Territory WHERE ID=" + id);

            foreach (DataRow elem in data.Rows)
            {
                int idHouse = (int)elem["idHouse"];
                House owner = DalManager.Instance.House.getById(idHouse);
                territory = new Territory((int)elem["Id"], (TerritoryType)Enum.Parse(typeof(TerritoryType), (string)elem["Type"]), owner, (string)elem["name"]);
            }

            return territory;
        }
        public List<Territory> getByHouseId(int id)
        {
            List<Territory> territories = new List<Territory>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Territory WHERE idHouse=" + id);

            foreach (DataRow elem in data.Rows)
            {
                int idHouse = (int)elem["idHouse"];
                House owner = DalManager.Instance.House.getById(idHouse);
                Territory territory = new Territory((int)elem["Id"], (TerritoryType)Enum.Parse(typeof(TerritoryType), (string)elem["Type"]), owner, (string)elem["name"]);
                territories.Add(territory);
            }

            return territories;
        }

        public void update(int id, string name, int idhouse, string type)
        {
            string request;
            DataTable dt;
            request = "SELECT Id, name, idHouse, Type FROM Territory WHERE Id=" + id;
            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if(id != -1 && dt.Rows.Count > 0)
            {
                dt.Rows[0]["name"] = name;
                dt.Rows[0]["idHouse"] = idhouse;
                dt.Rows[0]["Type"] = type;
            }
            else
            {
                string request_tmp = "SELECT MAX(Id) as idm FROM Territory";
                DataTable dt_tmp = DalManager.Instance.dal.selectByDataAdapter(request_tmp);
                id = ((int)dt_tmp.Rows[0]["idm"]) + 1;

                request = "SELECT Id, name, idHouse, Type FROM Territory";
                object[] obj = { id, name, idhouse, type };
                dt.Rows.Add(obj);
            }
            DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
        }

        public List<Tuple<int, int>> getAllAssociationById(int id)
        {
            List<Tuple<int, int>> list = new List<Tuple<int, int>>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT Id, idHouse FROM Territory WHERE idHouse = " + id);

            foreach (DataRow elem in data.Rows)
            {
                list.Add(new Tuple<int, int>((int)elem["Id"], (int)elem["idHouse"]));
            }

            return list;
        }
        public void delete(int id)
        {
            string request;
            DataTable dt;
            request = "SELECT Id, name, idHouse, Type FROM Territory WHERE Id=" + id;

            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if (dt.Rows.Count > 0)
            {
                dt.Rows[0].Delete();
                DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
            }
        }
    }

}