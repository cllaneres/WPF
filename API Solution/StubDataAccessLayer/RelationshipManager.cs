﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using System.Data;

namespace StubDataAccessLayer
{
    public class RelationshipManager
    {
        public RelationshipManager()
        {
        }

        public List<Tuple<Character, Character, RelationshipEnum>> getById(int id)
        {
            List<Tuple<Character, Character, RelationshipEnum>> relations = new List<Tuple<Character, Character, RelationshipEnum>>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Relationship WHERE Char1=" + id);

            foreach (DataRow elem in data.Rows)
            {
                Character char1 = DalManager.Instance.Character.getById((int)elem["Char1"]);
                Character char2 = DalManager.Instance.Character.getById((int)elem["Char2"]);
                relations.Add(new Tuple<Character, Character, RelationshipEnum>(char1, char2, (RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), (string)elem["Type"])));
            }

            data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Relationship WHERE Char2=" + id);

            foreach (DataRow elem in data.Rows)
            {
                Character char1 = DalManager.Instance.Character.getById((int)elem["Char1"]);
                Character char2 = DalManager.Instance.Character.getById((int)elem["Char2"]);
                relations.Add(new Tuple<Character, Character, RelationshipEnum>(char2, char1, (RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), (string)elem["Type"])));
            }

            return relations;
        }

        public List<Tuple<Character, Character, RelationshipEnum>> getAll()
        {
            List<Tuple<Character, Character, RelationshipEnum>> relations = new List<Tuple<Character, Character, RelationshipEnum>>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM Relationship");

            foreach (DataRow elem in data.Rows)
            {
                Character char1 = DalManager.Instance.Character.getById((int)elem["Char1"]);
                Character char2 = DalManager.Instance.Character.getById((int)elem["Char2"]);
                relations.Add(new Tuple<Character, Character, RelationshipEnum>(char1, char2, (RelationshipEnum)Enum.Parse(typeof(RelationshipEnum), (string)elem["Type"])));
            }

            return relations;
        }

        public void update(int id1, int id2, string type)
        {
            string request;
            DataTable dt;
            if (id1 != id2)
            {
                if (id1 < id2)
                    request = "SELECT Char1, Char2, Type FROM Relationship WHERE Char1=" + id1 + " AND Char2=" + id2;
                else
                    request = "SELECT Char1, Char2, Type FROM Relationship WHERE Char1=" + id2 + " AND Char2=" + id1;
                dt = DalManager.Instance.dal.selectByDataAdapter(request);
                if (dt.Rows.Count > 0)
                {
                    if (id1 < id2)
                    {
                        dt.Rows[0]["Char1"] = id1;
                        dt.Rows[0]["Char2"] = id2;
                    }
                    else
                    {
                        dt.Rows[0]["Char1"] = id2;
                        dt.Rows[0]["Char2"] = id1;
                    }
                    dt.Rows[0]["Type"] = type;
                }
                else
                {
                    request = "SELECT Char1, Char2, Type FROM Relationship";
                    int id1t;
                    int id2t;
                    if (id1 < id2)
                    {
                        id1t = id1;
                        id2t = id2;
                    }
                    else
                    {
                        id1t = id2;
                        id2t = id1;
                    }
                    object[] obj = { id1t, id2t, type };
                    dt = new DataTable();
                    dt.Columns.Add("Char1");
                    dt.Columns.Add("Char2");
                    dt.Columns.Add("Type");
                    dt.Rows.Add(obj);
                }
                DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
            }
        }
        public void delete(int id1, int id2)
        {
            string request;
            DataTable dt;
            if (id1 < id2)
                request = "SELECT Char1, Char2, Type FROM Relationship WHERE Char1=" + id1 + " AND Char2=" + id2;
            else
                request = "SELECT Char1, Char2, Type FROM Relationship WHERE Char1=" + id2 + " AND Char2=" + id1;

            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if (dt.Rows.Count > 0)
            {
                dt.Rows[0].Delete();
                DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
            }
        }

    }

}
