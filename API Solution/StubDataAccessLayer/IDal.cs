﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StubDataAccessLayer
{
    public interface IDal
    {
        DataTable selectByDataAdapter(string request);
        int UpdateByCommandBuilder(string request, DataTable table);
    }
}
