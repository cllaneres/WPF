﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntitiesLayer;
using System.Data;

namespace StubDataAccessLayer
{
    public class HouseManager
    {
        public HouseManager()
        {
        }

        public House getById(int id)
        {
            House house = null;
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM House WHERE ID=" + id);

            foreach (DataRow elem in data.Rows)
            {
                Character leader = null;
                int leader_num = ((elem["Leader"]).ToString().Equals("") ? -1 : (int)elem["Leader"]);
                if (leader_num != -1)
                    leader = DalManager.Instance.Character.getById(leader_num);
                house = new House((int)elem["Id"], (string)elem["Name"], leader, (int)elem["NumberOfUnity"]);
            }

            return house;
        }

        public List<House> getAll()
        {
            List<House> houses = new List<House>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT * FROM House");

            foreach (DataRow elem in data.Rows)
            {
                Character leader = null;
                int leader_num = ((elem["Leader"]).ToString().Equals("") ? -1 : (int)elem["Leader"]);
                if (leader_num != -1)
                    leader = DalManager.Instance.Character.getById(leader_num);
                House house = new House((int)elem["Id"], (string)elem["Name"], leader, (int)elem["NumberOfUnity"]);
                houses.Add(house);
            }

            return houses;
        }

        public List<House> getAllWithTerritoriesAndCharacter()
        {
            List<House> houses = new List<House>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT* FROM House WHERE Id IN(SELECT idHouse FROM Character) AND Id IN(SELECT idHouse FROM Territory)");

            foreach (DataRow elem in data.Rows)
            {
                Character leader = null;
                int leader_num = ((elem["Leader"]).ToString().Equals("") ? -1 : (int)elem["Leader"]);
                if (leader_num != -1)
                    leader = DalManager.Instance.Character.getById(leader_num);
                House house = new House((int)elem["Id"], (string)elem["Name"], leader, (int)elem["NumberOfUnity"]);
                houses.Add(house);
            }

            return houses;
        }

        public void update(int id, string name, int nOU, int leader)
        {
            string request;
            DataTable dt;
            request = "SELECT Id, Name, NumberOfUnity, Leader FROM House WHERE Id=" + id;
            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if(id != -1 && dt.Rows.Count > 0 )
            {
                dt.Rows[0]["Name"] = name;
                dt.Rows[0]["NumberOfUnity"] = nOU;
                dt.Rows[0]["Leader"] = leader;
            }
            else
            {
                string request_tmp = "SELECT MAX(Id) as idm FROM House";
                DataTable dt_tmp = DalManager.Instance.dal.selectByDataAdapter(request_tmp);
                id = ((int)dt_tmp.Rows[0]["idm"]) + 1;

                request = "SELECT Id, Name, NumberOfUnity, Leader FROM House";
                object[] obj = { id, name, nOU, leader };
                dt.Rows.Add(obj);
            }
            DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
        }

        public List<Tuple<int, int>> getAllAssociation()
        {
            List<Tuple<int, int>> list = new List<Tuple<int, int>>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT Id, idHouse FROM Character");

            foreach (DataRow elem in data.Rows)
            {
                list.Add(new Tuple<int, int>((int)elem["Id"], (int)elem["idHouse"]));
            }

            return list;
        }

        public List<Tuple<int, int>> getAllAssociationById(int id)
        {
            List<Tuple<int, int>> list = new List<Tuple<int, int>>();
            DataTable data = DalManager.Instance.dal.selectByDataAdapter("SELECT Id, idHouse FROM Character WHERE idHouse = " + id);

            foreach (DataRow elem in data.Rows)
            {
                list.Add(new Tuple<int, int>((int)elem["Id"], (int)elem["idHouse"]));
            }

            return list;
        }
        public void delete(int id)
        {
            string request;
            DataTable dt;
            request = "SELECT Id, Name, NumberOfUnity, Leader FROM House WHERE Id=" + id;

            dt = DalManager.Instance.dal.selectByDataAdapter(request);
            if (dt.Rows.Count > 0)
            {
                // Changer les character
                List<Character> characters = DalManager.Instance.Character.getByHouseId(id);
                foreach (Character chara in characters)
                {
                    DalManager.Instance.Character.update(chara.Id, chara.FirstName, chara.LastName, chara.Caracteristics.Bravoury, chara.Caracteristics.Pv, chara.Caracteristics.Crazyness, -1, chara.Caracteristics.Strength, chara.Caracteristics.Type.ToString());
                }

                // Changer les territories
                List<Territory> territories = DalManager.Instance.Territory.getByHouseId(id);
                foreach (Territory terr in territories)
                {
                    DalManager.Instance.Territory.update(terr.Id, terr.Name, -1, terr.TerritoryType.ToString());
                }

                dt.Rows[0].Delete();
                DalManager.Instance.dal.UpdateByCommandBuilder(request, dt);
            }
        }

    }

}